#include <Servo.h>

Servo v_servo;
Servo h_servo;

// step/speed to move to requested angle
const int step = 1;

// current angles for servo motors
int cur_v = 90;
int cur_h = 90;

// remotely requested angles for the motors
int target_v = 90;
int target_h = 90;

void setup() {
  v_servo.attach(9);  // Vertical plane motor is on pin 9
  h_servo.attach(10); // Horizontal plane motor is on pin 10
  Serial.begin(9600);
  delay(1000);
}

void loop() {
  // no jitter, move slowly to the targeted angles
  cur_v += cur_v > target_v
    ? max(target_v - cur_v, -step)
    : min(target_v - cur_v, step);
  cur_h += cur_h > target_h
    ? max(target_h - cur_h, -step)
    : min(target_h - cur_h, step);

  v_servo.write(cur_v);
  h_servo.write(cur_h);

  delay(25);
}

/*
 This event happens when there is some incoming
 data from serial is available. We expect the
 fragments like '{15,48}' to put them into
 'target_v=15' and 'target_h=48'.
*/
void serialEvent() {
  char ch;
  int v, h;
  while (Serial.available()) {
    ch = Serial.read();
    if(ch == '{') {
      // Read VERTICAL ANGLE first
      v = 0;
      while((ch = Serial.read()) != ',')
        v = (v * 10) + (ch - '0');

      // Now read HORIZONTAL ANGLE
      h = 0;
      while((ch = Serial.read()) != '}')
        h = (h * 10) + (ch - '0');

      // Update angles only if they are meaningful 
      if(0 <= v && v <= 180 && 0 <= h && h <= 180) {
        target_v = v;
        target_h = h;
      }

      // Send the confirmation back to PC
      Serial.print("ARDUINO:");
      Serial.print(target_v, DEC);
      Serial.print(";");
      Serial.print(target_h, DEC);
      Serial.println();
    }
  }
}
