import os
import sys
import serial
import time


def read_from_arduino(ser):
    """
    Reads the data from serial until no data is available.
    """
    while ser.inWaiting() > 0:
        b = ser.read()
        sys.stdout.write(b.decode())


def send_to_arduino(ser, x_angle, y_angle):
    """
    Sends the command to serial.
    """
    cmd = ('{%s,%s}' % (x_angle, y_angle)).encode()
    print 'Sending %s to Arduino...' % repr(cmd)
    ser.write(cmd)


def init_serial(com_port_path, speed):
    """
    Tries to initialize the serial connection
    in infinite loop until success.
    """
    while True:
        try:
            ser = serial.Serial(com_port_path, speed)
            break
        except Exception as e:
            print 'Exception while trying %s com port: %s' % (com_port_path, e)
            time.sleep(5)
    print 'Serial port %s is open successfully' % com_port_path
    
    return ser
