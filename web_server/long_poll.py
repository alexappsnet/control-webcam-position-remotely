from tornado.concurrent import Future

class LongPoll(object):
    """
    Manages the long polling for web client requests and refreshes
    them with the new webcam image once the image is available.
    """

    def __init__(self):
        self.waiters = set()
 
    def wait_for_image(self):
        result_future = Future()
        self.waiters.add(result_future)
        return result_future
 
    def cancel_wait(self, future):
        self.waiters.remove(future)
        future.set_result('')
 
    def new_image(self, image):
        size = len(self.waiters)
        if not size:
            return
            
        print 'Serving %d clients' % size
        for future in self.waiters:
            future.set_result(image)
        self.waiters = set()
