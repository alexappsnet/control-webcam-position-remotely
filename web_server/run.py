import os

import arduino_control
import long_poll
import webcam_control
import web_server

from tornado.ioloop import IOLoop

# Port to serve the incomming web requests
HTTP_PORT = 8001

# The camera port for host with a single webcam is usually 0
CAMERA_PORT = 0

# External webcam might have a different port number
#CAMERA_PORT = 1

# How often to refresh the image from webcam
IMAGE_REFRESH_INTERVAL = 0.25

# Path to Arduino on my Windows Destop
#SERIAL_COM_PORT_PATH = '\\.\COM4'

# Path to Arduino on my MacBook
SERIAL_COM_PORT_PATH = '/dev/tty.usbmodemfd121'

# Path to Arduino on my Linux
#SERIAL_COM_PORT_PATH = '/dev/ttyACM0'

# Path to Arduino on my MacBook with Ubuntu
#SERIAL_COM_PORT_PATH = '/dev/ttyACM0' # This works on my Ubuntu

# Serial speed
SERIAL_COM_PORT_SPEED = 9600


def main():
    app = web_server.make_app()
    app.long_poll = long_poll.LongPoll()
    app.ser = arduino_control.init_serial(
        SERIAL_COM_PORT_PATH,
    SERIAL_COM_PORT_SPEED)
    IOLoop.current().add_callback(
        web_server.check_arduino_data,
    app.ser)

    camera = webcam_control.init_webcam(CAMERA_PORT)
    IOLoop.current().add_callback(
        web_server.refresh_image,
    camera,
    app.long_poll,
    IMAGE_REFRESH_INTERVAL)

    print 'http://localhost:%d' % HTTP_PORT
    app.listen(HTTP_PORT)
    IOLoop.current().start()
 
 
if __name__ == "__main__":
    main()
