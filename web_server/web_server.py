import os
import time

import arduino_control
import webcam_control
 
from tornado.ioloop import IOLoop
from tornado.web import RequestHandler, Application, url
from tornado.gen import coroutine, Task
from tornado.concurrent import Future


class MainHandler(RequestHandler):
    def get(self):
        self.render("index.html")


class PositionHandler(RequestHandler):
    def post(self):
        x = int(self.get_argument('x', '0'))
        y = int(self.get_argument('y', '0'))
        print 'Got new position request: x=%d, y=%d' % (x, y)        
        self.send_to_arduino(x, y)
        self.write("OK")
        
    def send_to_arduino(self, x, y):
        # -50..+50 map to 180..0
        x = (50 - x) * 180 / 100 
        # -50..+50 map to 180..0
        y = (50 - y) * 180 / 100
        arduino_control.send_to_arduino(self.application.ser, x, y)


class ImgHandler(RequestHandler):
    """
    Waits for new jpeg image from webcam and sends it to the client
    web browser.
    """
    @coroutine
    def get(self,):
        yield Task(
            IOLoop.current().add_timeout,
            IOLoop.current().time() + 0.2)
        self.future = self.application.long_poll.wait_for_image()
        image = yield self.future
        if self.request.connection.stream.closed():
            return
        self.set_header('Content-type', 'image/jpg')
        self.write(image)

    def on_connection_close(self):
        self.application.long_poll.cancel_wait(self.future)
        

def make_app():
    return Application(
        [url(r"/", MainHandler),
         url(r"/img", ImgHandler),
         url(r"/new_position", PositionHandler),
         ],
        template_path=os.path.join(os.path.dirname(__file__), "template"),
        static_path=os.path.join(os.path.dirname(__file__), "static"),
        debug=True,
    )
        

@coroutine
def check_arduino_data(ser):
    while True:
        yield Task(
            IOLoop.current().add_timeout,
            IOLoop.current().time() + 100)
        arduino_control.read_from_arduino(ser)


@coroutine
def refresh_image(camera, long_poll, interval):
    """
    Gets the new images from webcam in infinite loop.
    """
    while True:
        yield Task(
            IOLoop.current().add_timeout,
            IOLoop.current().time() + interval)
        img = webcam_control.get_image(camera)
        jpeg = webcam_control.convert_to_jpeg(img)
    
        # update waiters
        long_poll.new_image(''.join(chr(x) for x in jpeg))
