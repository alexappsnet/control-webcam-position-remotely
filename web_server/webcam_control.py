import cv2


def get_image(camera):
    """
    Reads the raw image from webcam.
    """
    _, im = camera.read()
    return im
 
 
def init_webcam(port=0):
    """
    Initializes the web cam on given port. Port might need the adjustment.
    """
    camera = cv2.VideoCapture(port)
    for i in xrange(15):
        get_image(camera)
    return camera
 
 
def convert_to_jpeg(img):
    """
    Converts the image to jpeg in memory
    """
    encode_param = [int(cv2.IMWRITE_JPEG_QUALITY), 90]
    _, image = cv2.imencode('.jpg', img, encode_param)
 
    return image
